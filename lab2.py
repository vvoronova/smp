import numpy as np
from scipy.optimize import linprog


def solve(num, A1, A2, B1, B2, Ab, c1, c2, b0, b1, b2, db):
    while True:
        Ab_inverted = np.linalg.inv(Ab)
        u = db.dot(Ab_inverted)
        u_tr = u[:-1 * num]

        params = [
            (A1, B1, b1, c1, u[-2], np.array([1, 0])),
            (A2, B2, b2, c2, u[-1], np.array([0, 1]))
        ]
        costs = [u_tr.dot(A) + v - c for A, _, _, c, v, _ in params]

        solutions = [linprog(c=costs[i], A_eq=B, b_eq=b).x for i, (_, B, b, _, _, _) in enumerate(params)]

        vals = np.array([costs[i].dot(x) for i, x in enumerate(solutions)])

        min_ind = vals.argmin()

        if vals[min_ind] >= 0:
            break

        A_new = np.concatenate((params[min_ind][0].dot(solutions[min_ind]), params[min_ind][5]))
        d_new = params[min_ind][3].dot(solutions[min_ind])

        z = Ab_inverted.dot(A_new)

        t = np.array([Ab_inverted[i].dot(np.concatenate((b0, np.array([1, 1])))) / item
                      if item > 0 else float("inf")
                      for i, item in enumerate(z)])

        t_min_ind = t.argmin()
        if t[t_min_ind] == float("inf"):
            return

        Ab[:, t_min_ind] = A_new
        db[t_min_ind] = d_new

    return db.dot(np.linalg.inv(Ab)).dot(np.concatenate((b0, np.array([1] * num))))


if __name__ == '__main__':
    num = 2
    A1 = np.array([
        [1, 1, 0],
        [1, 0, 0],
    ], dtype=np.float64)
    A2 = np.array([
        [0, 0, -1],
        [0, -1, 0],
    ], dtype=np.float64)
    B1 = np.array([
        [1, 2, 1],
    ], dtype=np.float64)
    B2 = np.array([
        [1, 1, 1],
    ], dtype=np.float64)
    c1 = np.array([1, 1, 0], dtype=np.float64)
    c2 = np.array([0, 0, 0], dtype=np.float64)
    b0 = np.array([1, 1], dtype=np.float64)
    b1 = np.array([6], dtype=np.float64)
    b2 = np.array([4], dtype=np.float64)
    Ab = np.array([
        [0, 6, -4, 0],
        [0, 6, 0, 0],
        [1, 1, 0, 0],
        [0, 0, 1, 1],
    ], dtype=np.float64)
    db = np.array([0, 6, 0, 0], dtype=np.float64)

    res = solve(num, A1, A2, B1, B2, Ab, c1, c2, b0, b1, b2, db)
    print(res)
