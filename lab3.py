import numpy as np


def solve(A, B):
    results_a = np.zeros(shape=A.shape)
    results_b = np.zeros(shape=B.shape)

    for i, item in enumerate(np.argmax(A, axis=0)):
        results_a[item][i] = 1

    for i, item in enumerate(np.argmax(B, axis=1)):
        results_b[i][item] = 1

    result = np.logical_and(results_a, results_b)

    return list(zip(*np.where(result == True)))


if __name__ == '__main__':
    A = np.array([
        [2, 3, 7, 5],
        [1, 2, 5, 3],
    ])
    B = np.array([
        [7, 2, 5, 6],
        [9, 8, 4, 0],
    ])
    print(solve(A, B))

    A = np.array([
        [5, 1],
        [0, 4],
    ])
    B = np.array([
        [4, 1],
        [0, 5],
    ])
    print(solve(A, B))
